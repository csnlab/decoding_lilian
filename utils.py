import os
from constants import *
import pandas as pd


def get_session_info():
    area = 'V1'
    sessinfo = []
    for condition in conditions :
        path = os.path.join(DATA_FOLDER, 'decoding_data', area, condition,
                            'session_info.csv')

        sessinfo.append(pd.read_csv(path))
    si =  pd.concat(sessinfo)

    si = si.rename({'animal' : 'animal_id', 'mental_state' : 'condition'},
                   axis=1)

    sessid = []
    for i, row in si.iterrows() :
        date = pd.to_datetime(row['session_date']).strftime('%Y-%m-%d')
        sessid.append('{}_{}'.format(date, row['animal_id']))
    si['session_date'] = pd.to_datetime(si['session_date']).dt.strftime('%d/%m/%Y')
    si['session_id'] = sessid
    si.index = si['session_id']
    si = si.drop('session_id', axis=1)

    return si
