import os
from constants import *
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from plotting_style import *
import numpy as np

"""
Load decoding results per "experiment set" (i.e. per group of decoding contrasts).
"""


settings_name = 'may14_mean'
experiment_sets = ['stim_color', 'illusory_contour',
                   'filled_vs_outline']

experiment_sets = ['illusory_contour']

plot_format = 'png'
dpi = 200

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'decoding', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for experiment_set in experiment_sets:

    output_file_name = 'decode_{}_{}.pkl'.format(settings_name, experiment_set)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'decode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    df_all = out['df']
    pars = out['pars']
    session_ids = pars['session_ids']



    df = df_all.copy()

    da = df.copy().drop(['experiment'], axis=1)
    if np.isin(experiment_set, ['illusory_contour', 'filled_vs_outline']) :
        # averaging across stim color
        da = da.drop(['stim_color'], axis=1)
        da = da.groupby(['session_id', 'stimulus_type', 'condition']).mean().reset_index()
        order = ['moving_bar', 'square', 'rotated_square']

    elif np.isin(experiment_set, ['stim_color']) :
        da = da.drop(['stim_color'], axis=1)
        da = da.groupby(['session_id', 'stimulus_type', 'condition']).mean().reset_index()
        order = ['moving bar', 'moving bar outline', 'square', 'square outline',
                 'rotated square', 'rotated square outline', 'square dots',
                 'rotated square dots']

    f, ax = plt.subplots(1, 1, figsize=[6, 4])
    sns.barplot(data=da, x='accuracy_score',
                y='stimulus_type', order=order,
                hue='condition', hue_order=conditions,
                estimator='median',
                palette=condition_palette)
    ax.axvline(0.5, ls='--', c='grey')
    ax.set_xlim([0, 1])
    ax.set_ylabel('')
    ax.set_xlabel('Decoding accuracy - {}'
                  '\n(median of {} sessions)'.format(experiment_set,
                                                     len(session_ids)))
    sns.despine()
    plt.tight_layout()

    plot_name = 'decoding_barplot_condition_{}_{}.{}'.format(experiment_set,
                                                              settings_name,
                                                              plot_format)

    f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)











    # --- plot each individual experiment ---

    for condition in conditions:
        df = df_all[df_all['condition'] == condition]
        df = df.drop('condition', axis=1)

        f, ax = plt.subplots(1, 1, figsize=[6, 4])
        sns.barplot(data=df, x='accuracy_score', y='experiment', estimator='median',
                    color=state_palette['awake'])
        ax.axvline(0.5, ls='--', c='grey')
        ax.set_xlim([0, 1])
        ax.set_ylabel('')
        ax.set_xlabel('Decoding accuracy - {}'
                      '\n(median of {} sessions)'.format(experiment_set, len(session_ids)))
        sns.despine()
        plt.tight_layout()

        plot_name = 'decoding_barplot_{}_{}_{}.{}'.format(experiment_set, condition, settings_name,
                                                       plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)



        # --- average bar orientations ---


        da = df.copy().drop(['experiment'], axis=1)
        if np.isin(experiment_set, ['illusory_contour', 'filled_vs_outline']):
            da = da.groupby(['session_id', 'stimulus_type', 'stim_color']).mean().reset_index()
            order = ['moving_bar', 'square', 'rotated_square']
            hue = 'stim_color'
            hue_order = ['bw', 'black', 'white']
            palette = 'dark:#029386'
            color = None

        elif np.isin(experiment_set, ['stim_color']):
            da = da.drop(['stim_color'], axis=1)
            da = da.groupby(['session_id', 'stimulus_type']).mean().reset_index()
            order = ['moving bar', 'moving bar outline', 'square', 'square outline',
                     'rotated square', 'rotated square outline', 'square dots', 'rotated square dots']
            hue = None
            hue_order = None
            palette = None
            color = sns.xkcd_rgb['teal']

        f, ax = plt.subplots(1, 1, figsize=[6, 4])
        sns.barplot(data=da, x='accuracy_score',
                    y='stimulus_type', order=order,
                    hue=hue, hue_order=hue_order,
                    estimator='median',
                    palette=palette, color=color)
        ax.axvline(0.5, ls='--', c='grey')
        ax.set_xlim([0, 1])
        ax.set_ylabel('')
        ax.set_xlabel('Decoding accuracy - {}'
                      '\n(median of {} sessions)'.format(experiment_set, len(session_ids)))
        sns.despine()
        plt.tight_layout()

        plot_name = 'decoding_barplot_grouped_{}_{}_{}.{}'.format(experiment_set, condition,
                                                                  settings_name,
                                                               plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)


