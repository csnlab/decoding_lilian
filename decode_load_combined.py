import os
from constants import *
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from plotting_style import *
import numpy as np
import pandas as pd
from utils import *

"""
Load decoding results aggregating across decodings which look at different
visual features
"""


settings_name = 'may14_mean'
experiment_sets = ['stim_color', 'illusory_contour',
                   'filled_vs_outline']

plot_format = 'png'
dpi = 200

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'decoding', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


si = get_session_info()

dfs = []
for experiment_set in experiment_sets:

    output_file_name = 'decode_{}_{}.pkl'.format(settings_name, experiment_set)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'decode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    df = out['df']
    pars = out['pars']
    session_ids = pars['session_ids']

    da = df.copy()
    # if experiment_set == 'illusory_contour':
    #     da = da[da['stimulus_type'] == 'moving_bar']

    # average across stimulus types, colors, orientation... everything!
    da = da.groupby(['session_id', 'condition']).mean(numeric_only=True).reset_index()

    da['experiment_set'] = experiment_set
    dfs.append(da)

df = pd.concat(dfs)

dp = df.pivot(index='session_id', columns='experiment_set', values='accuracy_score')

dp['condition'] = [si.loc[i, 'condition'] for i in dp.index]
dp['n_neurons'] = [si.loc[i, 'n_neurons'] for i in dp.index]


for experiment_set_2 in ['stim_color', 'filled_vs_outline']:
    f, ax = plt.subplots(1, 1, figsize=[4, 4])
    dot_scaling = 3
    for condition in conditions:
        dx = dp[dp['condition'] == condition]
        ax.scatter(dx[experiment_set_2],
                   dx['illusory_contour'],
                   s=dx['n_neurons']/dot_scaling,
                   c=condition_palette[condition],
                   label=condition)
    ax.set_xlabel('Decoding accuracy - {}'.format(experiment_set_2))
    ax.set_ylabel('Decoding accuracy - {}'.format('illusory_contour'))

    ax.set_xlim([0.4, 1.05])
    ax.set_ylim([0.4, 1.05])
    ax.axhline(0.5, ls=':', c='grey')
    ax.axvline(0.5, ls=':', c='grey')
    ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls=':', c='grey')
    ax.legend(loc='upper left', frameon=False)
    sns.despine()
    ax.set_aspect('equal')
    plt.tight_layout()

    plot_name = 'decoding_scatter_{}_vs_{}_{}.{}'.format('illusory',
                                                         experiment_set_2,
                                                          settings_name,
                                                          plot_format)

    f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)


