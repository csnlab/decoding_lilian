import os
from constants import *
import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import pickle
from utils import *

settings_name = 'may14_mean'
area = 'V1'
response_type = 'mean' # peak or mean
n_splits = 3
n_repeats = 5
n_estimators = 200 # number of trees for the random forest

# option to shuffle trial order at each repeat
shuffle_trial_order = True


experiments = 'all'#['square_illusory_bw']

experiment_sets = ['stim_color', 'illusory_contour',
                   'filled_vs_outline']

# TODO if you add catch trials, balance

stim_type_labels = {'sq_full' : 'square',
                    'sq_out'  : 'square outline',
                    'sq_full_rot' : 'rotated square',
                    'sq_out_rot' : 'rotated square outline',
                    'cont_ind' : 'square dots',
                    'cont_ind_rot' : 'rotated square dots',
                    'mv_bar_fill' : 'moving bar',
                    'mv_bar_out' : 'moving bar outline'}

for experiment_set in experiment_sets:
    # --- SELECT RECORDING ---

    si = get_session_info()
    session_ids = si.index.values
    #session_ids = si[si['condition'] == condition].index.values

    # --- GENERATE EXPERIMENTS ---
    # since we have many options, we generate a dataframe with all the
    # possible decoding contrasts

    ef = pd.DataFrame(columns=['stimulus_type', 'trial_type_1', 'trial_type_2',
                               'stim_color_1', 'stim_color_2', 'trial_orientation'])

    if experiment_set == 'illusory_contour':
        for color in ['bw', 'black', 'white'] :
            for orientation in [0, 90, 180, 270]:
                experiment_name = 'mv_bar_illusory_{}_{}'.format(orientation, color)
                ef.loc[experiment_name, :] = ['moving_bar', 'mv_bar_ill',
                                              'mv_bar_rot', color, color, orientation]

        for color in ['bw', 'black', 'white']:

            experiment_name = 'square_illusory_{}'.format(color)
            ef.loc[experiment_name, :] = ['square', 'sq_ill', 'rot_ind', color, color, None]

            experiment_name = 'rotated_square_illusory_{}'.format(color)
            ef.loc[experiment_name, :] = ['rotated_square', 'sq_ill_rot', 'rot_ind_rot',
                                          color, color, None]


    elif experiment_set == 'filled_vs_outline':

        for color in ['bw', 'black', 'white'] :
            for orientation in [0, 90, 180, 270]:
                experiment_name = 'mv_bar_filled_vs_outline_{}_{}'.format(orientation, color)
                ef.loc[experiment_name, :] = ['moving_bar', 'mv_bar_fill',
                                              'mv_bar_out', color, color, orientation]

        for color in ['bw', 'black', 'white']:

            experiment_name = 'square_filled_vs_outline_{}'.format(color)
            ef.loc[experiment_name, :] = ['square', 'sq_full', 'sq_out', color, color, None]

            experiment_name = 'rotated_square_filled_vs_outline_{}'.format(color)
            ef.loc[experiment_name, :] = ['rotated_square', 'sq_full_rot', 'sq_out_rot',
                                          color, color, None]



    elif experiment_set == 'stim_color':

        # moving stimuli
        stim_types = ['mv_bar_fill', 'mv_bar_out']

        for stim_type in stim_types:
            for orientation in [0, 90, 180, 270]:
                experiment_name = '{} {} color'.format(stim_type, orientation)

                ef.loc[experiment_name, :] = [stim_type_labels[stim_type],
                                              stim_type, stim_type, 'black', 'white', orientation]

        # static stimuli
        stim_types = ['sq_full', 'sq_out', 'sq_full_rot', 'sq_out_rot',
                      'cont_ind', 'cont_ind_rot']
        experiment_names = ['{} color'.format(stim_type_labels[s]) for s in stim_types]

        for stim_type, experiment_name in zip(stim_types, experiment_names):
            ef.loc[experiment_name, :] = [stim_type_labels[stim_type],
                                          stim_type, stim_type, 'black', 'white', None]

    if experiments != 'all':
        ef = ef.loc[experiments]

    # --- OUTPUT FILES ---------------------------------------------------------
    output_file_name = 'decode_{}_{}.pkl'.format(settings_name, experiment_set)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'decode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)


    # --- DECODE ---
    print('\n decoding:')
    print(session_ids)

    df = pd.DataFrame(columns=['session_id', 'experiment', 'accuracy_score',
                               'stimulus_type', 'stim_color', 'condition', 'n_neurons'])

    for experiment, exp_specs in ef.iterrows():

        print('\n__ decoding experiment {}'.format(experiment))


        for session_id in session_ids:

            # --- LOAD DATA ---

            condition = si.loc[session_id, 'condition']
            n_neurons = si.loc[session_id, 'n_neurons']

            path = os.path.join(DATA_FOLDER, 'decoding_data', area, condition)

            trial_data_path = os.path.join(path, 'trial_data',
                                           '{}_trial_data.csv'.format(session_id))

            data_path = os.path.join(path, '{}_response'.format(response_type),
                                     '{}_{}_responses.csv'.format(session_id, response_type))

            tf = pd.read_csv(trial_data_path)
            ds = pd.read_csv(data_path)

            # --- DATA PREPROCESSING ---

            tf['stim_color'] = tf['stim_color'].astype(str) # catch trials: 'nan' color
            tf = tf.drop('trial_type', axis=1)
            tf = tf.rename({'trial_type_str' : 'trial_type'}, axis=1)
            tf['trial_id'] = tf['trial_num'].astype(str)
            tf = tf.drop('trial_num', axis=1)
            tf.index = tf['trial_id']
            tf = tf.drop('trial_id', axis=1)
            tf['session_id'] = session_id
            tf = tf.drop('session', axis=1)

            ds['trial_id'] = tf.index
            ds.index = ds['trial_id']
            ds = ds.drop('trial_id', axis=1)

            # --- TRIAL SELECTION ---
            ts = tf.copy()
            if exp_specs['trial_orientation'] is not None:
                ts = ts[ts['trial_orientation'] == exp_specs['trial_orientation']]

            t1 = ts[ts['trial_type'] == exp_specs['trial_type_1']]
            t2 = ts[ts['trial_type'] == exp_specs['trial_type_2']]

            if exp_specs['stim_color_1'] != 'bw':
                t1 = t1[t1['stim_color'] == exp_specs['stim_color_1']]
            if exp_specs['stim_color_2'] != 'bw' :
                t2 = t2[t2['stim_color'] == exp_specs['stim_color_2']]

            if exp_specs['stim_color_1'] == exp_specs['stim_color_2']:
                stim_color_df = exp_specs['stim_color_1']
            else:
                stim_color_df = '{}_vs_{}'.format(exp_specs['stim_color_1'],
                                                  exp_specs['stim_color_2'])

            scores = []
            for repeat in range(n_repeats):

                tids1 = t1.index.values
                tids2 = t2.index.values

                # every repeat, we shuffle the original trial order
                if shuffle_trial_order:
                    rng = np.random.default_rng(seed=repeat)
                    tids1 = rng.choice(tids1, len(tids1), replace=False)
                    tids2 = rng.choice(tids2, len(tids2), replace=False)

                X = np.vstack([ds.loc[tids1].values, ds.loc[tids2].values])
                y = np.hstack([np.zeros(len(tids1)), np.ones(len(tids2))])

                kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                        random_state=repeat)

                iterable = kfold.split(X, y)

                cv_scores = []

                for fold, (training_ind, testing_ind) in enumerate(iterable) :
                    # decoder = SGDClassifier(random_state=92)
                    # decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                    X_train = X[training_ind, :]
                    X_test = X[testing_ind, :]
                    y_train = y[training_ind]
                    y_test = y[testing_ind]

                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                    model = RandomForestClassifier(n_estimators=n_estimators)
                    model.fit(X_train, y_train)

                    y_pred = model.predict(X_test)

                    fold_score = accuracy_score(y_true=y_test, y_pred=y_pred)

                    cv_scores.append(fold_score)

                scores.append(np.mean(cv_scores))

            score_mean = np.mean(scores)
            score_std = np.std(scores)

            row = [session_id, experiment, score_mean,
                   exp_specs['stimulus_type'], stim_color_df, condition, n_neurons]

            df.loc[df.shape[0], :] = row

            print('____ session {}, number of trials = [{}, {}]'.format(session_id, len(tids1), len(tids2)))
            print('________ mean score = {:.2f} +- {:.2f}'.format(score_mean, score_std))


    df['accuracy_score'] = pd.to_numeric(df['accuracy_score'])


    pars = {'settings_name' : settings_name,
            'area' : area,
            'condition' : condition,
            'response_type' : response_type,
            'n_splits' : n_splits,
            'n_repeats' : n_repeats,
            'shuffle_trial_order' : shuffle_trial_order,
            'session_ids' : session_ids,
            'ef' : ef}


    out = {'pars' : pars,
           'df' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))



