import seaborn as sns

state_palette = {'awake' : sns.xkcd_rgb['teal']}


color_palette = {'bw' : sns.xkcd_rgb['grey'],
                 'black' : sns.xkcd_rgb['dark grey'],
                 'white' : sns.xkcd_rgb['light grey']}

condition_palette = {'awake' : sns.xkcd_rgb['pumpkin orange'],
                     'anesthetized' : sns.xkcd_rgb['dusty teal']}